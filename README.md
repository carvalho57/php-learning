# php-learning



## Roadmap

### Getting Familiar with PHP: Syntax, functions, dates, JSON, File System, CURL
#### OOP, Namespaces and Composer

[PHP Tutorial for Absolute Beginners - PHP Course 2021](https://www.youtube.com/playlist?list=PLr3d3QYzkw2xabQRUpcZ_IBk9W50M9pe-)


### Projetos (Udemy)
* Site Institucional
* Catalogo de produtos
* Gerenciador de gastos pessoais
* Blog
* Loja

### PHP beginner projects
* [ ] [Simple CRUD application with PHP and JSON](https://youtu.be/DWHZSkn5paQ)
* [ ] [Todo app with PHP](https://youtu.be/NxeNqHdJFxs)
* [ ] [PHP Watch files and directories recursively](https://youtu.be/5f4PjQJI-Fc)
* [ ] [Bulk image resize with PHP](https://youtu.be/Z99lYtn3quk)
* [ ] [PHP registration form & validation](https://youtu.be/V5sJ76T3mWg)


### Build Projects
* [ ] [Build PHP Form Widget using OOP Abstraction](ttps://youtu.be/sBP6HKRW0sM)
* [ ] [Build Shopping Cart with OOP](ttps://youtu.be/1Ip7_hdSqzY)
* [ ] [Create MySql Notes App](ttps://youtu.be/DOsuFRnBqLU)
* [ ] [How To Create A Login System In PHP For Beginners](ttps://youtu.be/gCo6JqGMi30)

### Build custom MVC Framework

 Build PHP MVC Framework https://www.youtube.com/watch?v=WKy-N0q3WRo&list=PLLQuc_7jk__Uk_QnJMPndbdKECcTEwTA1 


### What is API

What is an API?                                                    https://youtu.be/s7wmiS2mSXY 
APIs for Beginners - How to use an API (Full Course / Tutorial)    https://youtu.be/GZvSYJDk-us 
Build PHP REST API From Scratch  https://www.youtube.com/watch?v=OEWXbpUMODk&list=PLillGF-RfqbZ3_Xr8do7Q2R752xYrDRAo

### Laravel 

 Laravel E-Commerce  https://www.youtube.com/watch?v=o5PWIuDTgxg&list=PLEhEHUEU3x5oPTli631ZX9cxl6cU_sDaR 
 Laravel Movie App  https://www.youtube.com/watch?v=9OKbmMqsREc&list=PLEhEHUEU3x5pYTjZze3fhYMB4Nl_WOHI4 
