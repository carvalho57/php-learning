<?php 

# https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/sessions/Session

session_start();
$_SESSION['index'] = "Index"

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Index</title>
</head>
<body>
    <h3>Index</h3>
    <?php echo  (isset($_SESSION['servico']) ? $_SESSION['servico'] : 'nada')?>
    <a href="servico.php">Serviços</a>
</body>
</html>