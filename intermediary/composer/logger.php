<?php

require_once(dirname(__FILE__) . '/vendor/autoload.php');



use Monolog\Logger;
use Monolog\Handler\StreamHandler;

function logger(string $message, $modo = 'info')
{
    $logger = new Logger('logs');
    $logger->pushHandler(new StreamHandler(dirname(__FILE__) . '/logs.txt'));

    switch ($modo) {
        case 'info':
            $logger->info($message);
            break;
        case 'warning':
            $logger->warning($message);
            break;
        case 'error':
            $logger->error($message);
            break;
        case 'debug':
            $logger->debug($message);
            break;
        case 'critical':
            $logger->critical($message);
            break;
        case 'alert':
            $logger->alert($message);
            break;
        case 'emergency':
            $logger->emergency($message);
            break;
        default:
            $logger->info($message);        
    }
}
