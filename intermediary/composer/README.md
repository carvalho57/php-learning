Composer
Composer is a tool for dependency management in PHP. It allows you to declare the libraries your project depends on and it will manage (install/update) them for you.

# Composer Repository
[Repositorio](https://packagist.org/)

composer.json -> dependencias do projeto

Iniciar um composer
```sh
 composer init
 ```

Instalar dependencias com base no composer.json
```sh
 composer install
 ```
Adicionar package
```sh
composer require package.name
```

Remove package

```sh
 composer remove package.name
```