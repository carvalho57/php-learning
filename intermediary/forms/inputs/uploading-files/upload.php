<?php

#https://www.php.net/manual/en/function.move-uploaded-file.php

// print_r($_POST); # nao vem nada

echo "<pre>";
print_r($_FILES); # super-global que armazena os arquivos submetidos para o servidor


$accepted_mimetypes = [
    'image/png', 'image/jpg'
];

$final = '/home/carvalho57/dev/php-learning/intermediary/forms/inputs/uploading-files/temp/';

foreach($_FILES as $file)
{
    print_r($file);

    if(!in_array($file['type'], $accepted_mimetypes)) continue;

    if($file['size'] > 5000000) continue;

    print_r(getimagesize($file['tmp_name'])); #dimensoes


    move_uploaded_file($file['tmp_name'], $final . $file['name']);
}

