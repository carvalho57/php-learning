<?php

namespace core\classes;

use Exception;
use PDO;
use PDOException;

class Database {
    
    private ?PDO $Connection;
    private string $database_host = MYSQL_SERVER;
    private string $database_name = MYSQL_DATABASE;
    private string $database_user = MYSQL_USER;
    private string $database_password = MYSQL_PASS;
    private string $database_charset = MYSQL_CHARSET;

    private function Connect()
    {        
        $dsn = "mysql:host={$this->database_host};dbname={$this->database_name};charset={$this->database_charset}";
        $this->Connection = new PDO($dsn,$this->database_user, $this->database_password,array(PDO::ATTR_PERSISTENT => true));
        $this->Connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
    }

    private function Disconnect() { $this->Connection = null;}

    public function select($statement, $params = null)
    {
                
        if(!preg_match("/^SELECT/i", trim($statement))) {
            throw new Exception('Database - Não é uma instrução select');
        }

        $this->Connect();

        $result = null;
        try{

            if(!empty($params))
            {
                $command = $this->Connection->prepare($statement);
                $command->execute($params);
                $result = $command->fetchAll(PDO::FETCH_CLASS);
            }else {
                $command = $this->Connection->prepare($statement);
                $command->execute();
                $result = $command->fetchAll(PDO::FETCH_CLASS);
            }


        }catch(PDOException $ex){
            return false;
        }

        $this->Disconnect();
        return $result;
    }

    public function insert($statement, $params = null)
    {
                
        if(!preg_match("/^INSERT/i", trim($statement))) {
            throw new Exception('Database - Não é uma instrução INSERT');
        }

        $this->Connect();
        
        try{

            if(!empty($params))
            {
                $command = $this->Connection->prepare($statement);
                $command->execute($params);                
            }else {
                $command = $this->Connection->prepare($statement);
                $command->execute();
            }


        }catch(PDOException $ex){
            return false;
        }

        $this->Disconnect();
    }

    public function update($statement, $params = null)
    {
                
        if(!preg_match("/^UPDATE/i", trim($statement))) {
            throw new Exception('Database - Não é uma instrução UPDATE');
        }

        $this->Connect();
        
        try{

            if(!empty($params))
            {
                $command = $this->Connection->prepare($statement);
                $command->execute($params);                
            }else {
                $command = $this->Connection->prepare($statement);
                $command->execute();
            }


        }catch(PDOException $ex){
            return false;
        }

        $this->Disconnect();
    }

    
    public function delete($statement, $params = null)
    {
                
        if(!preg_match("/^DELETE/i", trim($statement))) {
            throw new Exception('Database - Não é uma instrução DELETE');
        }

        $this->Connect();
        
        try{

            if(!empty($params))
            {
                $command = $this->Connection->prepare($statement);
                $command->execute($params);                
            }else {
                $command = $this->Connection->prepare($statement);
                $command->execute();
            }


        }catch(PDOException $ex){
            return false;
        }

        $this->Disconnect();
    }

    public function query($statement, $params = null)
    {
                
        if(!preg_match("/^(SELECT|INSERT|UPDATE|DELETE)/i", trim($statement))) {
            throw new Exception('Database - Instrução inválida');
        }

        $this->Connect();
        
        try{

            if(!empty($params))
            {
                $command = $this->Connection->prepare($statement);
                $command->execute($params);                
            }else {
                $command = $this->Connection->prepare($statement);
                $command->execute();
            }


        }catch(PDOException $ex){
            return false;
        }

        $this->Disconnect();
    }
}


/*

define('MYSQL_DATABASE', 'store');
define('MYSQL_USER', 'user');
define('MYSQL_PASS', 'P@ssw0rd');
define('MYSQL_CHARSET', 'utf-8');
*/