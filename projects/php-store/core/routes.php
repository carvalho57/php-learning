<?php

//tabela rotas
$routes = [
    'inicio' => 'main@index',
    'loja' => 'main@loja',
    'carrinho' => 'loja@carrinho'
];

// action padrao
$action = 'inicio';

if(isset($_GET['a']))
{
    if(key_exists($_GET['a'],$routes)) {
       $action = $_GET['a'];
    }
}

$partions = explode("@",$routes[$action]);
$controller = 'core\\controllers\\' . ucfirst($partions[0]);

$action = $partions[1];

$endpoint = new $controller();
$endpoint->$action();
