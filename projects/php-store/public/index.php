<?php

use core\classes\Database;

// abrir a sessao
session_start();

/*
carrega o config
carregar classes
carrega sistema de rotas
    - mostrar loja
    - mostrar carrinho
    - mostrar o backoffice, etc
*/

require_once('../config.php');
require_once('../vendor/autoload.php');

require_once('../core/routes.php');
