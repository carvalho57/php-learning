<?php

if (!isset($_GET['id'])) {
    die('Acesso negado');
}

use EasyPDO\EasyPDO;

require_once('includes/Config.php');
require_once('includes/EasyPDO.php');
require_once('includes/Encrypt.php');


$database_context = new EasyPDO(MYSQL_CONNECTION);
$contact_id = $_GET['id'];
$contact_id_decrypted = aes_decrypt($contact_id);

if ($contact_id_decrypted == false || $contact_id_decrypted == -1)
    die("Acesso negado");


$contact = $database_context->select('SELECT * FROM `Contacts` WHERE id = :id', [':id' => $contact_id_decrypted])[0];
//Retornar um vetor de arrays, mesmo q u

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar Contato</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
</head>

<body>
    
    <div class="container column is-3">
        <h3 class="title">Editar Contato: </h3>
        <form action="edit_submit.php" method="post">
            <div>
                <input type="hidden" name="id" value="<?= $contact_id ?>">
            </div>
            <div class="field">
                <label id="nome" class="label">Nome</label>
                <div class="control">
                    <input class="input" id="nome" name="nome" type="text" value="<?= $contact['nome'] ?>" maxlength="50">
                </div>
            </div>
            <div class="field">
                <label id="telefone" class="label">Telefone</label>
                <div class="control">
                    <input class="input" id="telefone" name="telefone"type="text" value="<?= $contact['telefone'] ?>" maxlength="30">
                </div>
            </div>
            <div>
                <input class="button is-success" type="submit" value="Editar">
                <a class="button is-info" href="index.php">Voltar</a>
            </div>
        </form>
</body>

</html>