<?php

require_once('Config.php');

function aes_encrypt($valor)
{
    return bin2hex(openssl_encrypt($valor, 'aes-256-cbc', AES_KEY, OPENSSL_RAW_DATA, AES_IV));
}

function aes_decrypt($hash)
{   //se não for par
    if(strlen($hash) % 2 != 0) #if(!(strlen($hash) & 0)) 
        return -1;

    return openssl_decrypt(hex2bin($hash), 'aes-256-cbc', AES_KEY, OPENSSL_RAW_DATA, AES_IV);
}