<?php

if($_SERVER['REQUEST_METHOD'] != 'POST')  {
    die('Acesso Negado');
}

$nome = $_POST['nome'];
$telefone = $_POST['telefone'];

use EasyPDO\EasyPDO;

require_once('includes/Config.php');
require_once('includes/EasyPDO.php');


$database_context = new EasyPDO(MYSQL_CONNECTION);

$params = [
    ':nome' => $nome,
    ':telefone' => $telefone
];

$database_context->insert("INSERT INTO `Contacts` (nome, telefone) VALUES (:nome,:telefone)", $params);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contato Removido</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">

</head>

<body>
    <div class="notification is-success">        
        <h3 class="title">Contato criado com sucesso!</h3>
        <a href="index.php">Retornar ao index</a>
    </div>
    
</body>
</html>

