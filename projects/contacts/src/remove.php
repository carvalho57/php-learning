<?php

if (!isset($_GET['id'])) {
    die('Acesso negado');
}

use EasyPDO\EasyPDO;

require_once('includes/Config.php');
require_once('includes/EasyPDO.php');
require_once('includes/Encrypt.php');

$database_context = new EasyPDO(MYSQL_CONNECTION);
$contact_id = aes_decrypt($_GET['id']);

$database_context->delete('DELETE FROM `Contacts` WHERE id = :id', [':id' => $contact_id]);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contato Removido</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">

</head>

<body>
    <div class="notification is-success">
        <h3 class="title">Contato removido com sucesso!</h3>
        <a href="index.php">Voltar ao index</a>
    </div>
</body>

</html>