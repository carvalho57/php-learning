<?php

    use EasyPDO\EasyPDO;

    require_once ('includes/Config.php');
    require_once ('includes/EasyPDO.php');
    require_once ('includes/Encrypt.php');


    $database_context = new EasyPDO(MYSQL_CONNECTION);

    $contacts = $database_context->select("SELECT * FROM `Contacts`");    

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contacts</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
</head>
<body>    
    <h3 class="title">Contatos</h3>
    <div class="container">
        <h4><a href="create.php">Novo Contato</a></h4>
        <table class="table  is-bordered is-striped">
            <tr>                
                <th>Nome</th>      
                <th>Telefone</th>
                <th></th>
            </tr>
            <?php foreach ($contacts as $contact) : ?>
                <tr>                
                    <td><?= $contact['nome'] ?></td>                    
                    <td><?= $contact['telefone'] ?></td>
                    <td>
                        <?php $id_encrypt = aes_encrypt($contact['id']) ?>
                        <a href="edit.php?id=<?= $id_encrypt ?>"> Editar</a>| 
                        <a href="remove.php?id=<?= $id_encrypt ?>">Remover</a>
                    </td>
                </tr>
            <?php endforeach ?>
        </table>
    </div>
</body>
</html>