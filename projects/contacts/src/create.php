<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Adicionar Contatos</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
</head>

<body>
    <div class="container column is-3">
        <h3 class="title">Novo Contato: </h3>
        <form action="create_submit.php" method="post">
            <div class="field">
                <label id="nome" class="label">Nome</label>
                <div class="control">
                    <input class="input" id="nome" name="nome" type="text" placeholder="Nome" maxlength="50">
                </div>
            </div>
            <div class="field">
                <label id="telefone" class="label">Telefone</label>
                <div class="control">
                    <input class="input" id="telefone" name="telefone" type="text" placeholder="Telefone" maxlength="30">
                </div>
            </div>
            <div>
                <input class="button is-success" type="submit" value="Adicionar">
                <a class="button is-info" href="index.php">Voltar</a>
            </div>
        </form>
    </div>
</body>

</html>