<?php


$todoName = $_POST['todo_name'] ?? '';
if($todoName)
{
    $todos = json_decode(file_get_contents('todo.json'),true);
    unset($todos[$todoName]);
    file_put_contents('todo.json',json_encode($todos, JSON_PRETTY_PRINT));        
    header('Location: index.php');
}

