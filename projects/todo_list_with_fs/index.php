<?php

$todos = file_exists('todo.json') ? json_decode(file_get_contents('todo.json'), true) : [];
// print_r($todos);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Index</title>
</head>

<body>

    <form action="newtodo.php" method="post">
        <input type="text" name="todo_name" placeholder="Enter your todo">
        <button type="submit">New Todo</button>
    </form>
    <ul>
        <?php foreach ($todos as $todoname => $todo) : ?>
            <li>
                <form style="display:inline-block" action="change_status.php" method="post">                    
                    <input type="hidden" name='todo_name'  value="<?= $todoname ?>">
                    <input type="checkbox" <?= $todo['completed'] ? 'checked' : '' ?>>
                </form>
                <span> <?php echo ucwords($todoname) ?></span>
                <form style="display:inline-block" action="delete.php" method="post">
                    <input type="hidden" name='todo_name' value="<?= $todoname ?>">
                    <button type="submit">Delete</button>
                </form>
            </li>
        <?php endforeach; ?>
    </ul>

    <script>
        const checkbox = document.querySelectorAll('input[type=checkbox]');
        checkbox.forEach(ch => {
            ch.onclick = function() {
                this.parentNode.submit();
            }
        })

    </script>
</body>

</html>