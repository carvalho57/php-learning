<?php

function SanitizeField($field_name)
{
    if(!isset($_POST[$field_name])) return false;
    
    return htmlspecialchars(stripslashes(trim($_POST[$field_name])));

}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $todo_name = SanitizeField('todo_name');

    if($todo_name)
    {        
        $todos = file_exists('todo.json') ? json_decode(file_get_contents('todo.json'),true) : [];
        $todos[$todo_name] = ['completed' => false];
        
        file_put_contents('todo.json',json_encode($todos, JSON_PRETTY_PRINT));        
    }
}


header('Location: index.php');