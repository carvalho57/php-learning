<?php


$repository = require_once('NotesRepository.php');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $currentNote =
        [
            'id' => '',
            'title' => '',
            'description' => ''
        ];

    if (isset($_GET['id'])) {
        $currentNote = $repository->getNoteById($_GET['id'])[0];
    } else
        $notes = $repository->getNotes();
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="app.css">
</head>

<body>
    <div>
        <form class="new-note" action="save.php" method="post">
            <input type="hidden" name="id" value="<?= $currentNote['id'] ?>">
            <input type="text" name="title" value="<?= $currentNote['title'] ?>" placeholder="Note title" autocomplete="off">
            <textarea name="description" cols="30" rows="4" placeholder="Note Description"><?= $currentNote['description'] ?></textarea>
            <button>
                <?php if ($currentNote['id']) : ?>
                    Update Note
                <?php else : ?>
                    New Note
                <?php endif; ?>
            </button>
        </form>
        <div class="notes">
            <?php foreach ($notes as $note) : ?>
                <div class="note">
                    <div class="title">
                        <a href="?id=<?= $note['id'] ?>"><?= $note['title'] ?></a>
                    </div>
                    <div class="description">
                        <?= $note['description'] ?>
                    </div>
                    <small><?= $note['create_date'] ?></small>
                    <form style="display:inline-block" action="delete.php" method="post">
                        <input type="hidden" name="id" value="<?= $note['id'] ?>">
                        <button class="close">X</button>
                    </form>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</body>

</html>