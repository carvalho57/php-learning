<?php


class NotesRepository
{
    private PDO $pdo;


    public function __construct()
    {
        $this->pdo = new PDO('mysql:host=db;dbname=notes', 'user', 'P@ssw0rd');
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function getNotes()
    {
        $command = $this->pdo->prepare("SELECT * FROM notes ORDER BY create_date DESC");
        $command->execute();
        return $command->fetchAll(PDO::FETCH_ASSOC);
    }

    public function addNote($note) 
    {
        $params = [
            ':title' => $note['title'],
            ':description' => $note['description'],            
        ];

        $command = $this->pdo->prepare("INSERT INTO `notes`(`title`, `description`, `create_date`) VALUES (:title,:description,:create_date)");
        $command->execute($params);        
    }

    public function getNoteById($noteid) 
    {
        $command = $this->pdo->prepare("SELECT * FROM notes WHERE id = :id");
        $command->bindValue('id', $noteid);
        $command->execute();        
        return $command->fetchAll(PDO::FETCH_ASSOC);
    }

    public function updateNote($note) 
    {
        $params = [
            ':id' => $note['id'],
            ':title' => $note['title'],
            ':description' => $note['description'],            
        ];


        $command = $this->pdo->prepare("UPDATE notes SET title=:title,description=:description WHERE id=:id");        
        $command->execute($params);                
    }

    public function deleteNote($noteId) 
    {        
        $command = $this->pdo->prepare("DELETE FROM notes WHERE id=:id");        
        $command->bindValue('id', $noteId);
        $command->execute();                       
    }
}

return new NotesRepository();