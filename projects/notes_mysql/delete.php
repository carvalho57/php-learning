<?php


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $repository = require_once('NotesRepository.php');
    $noteId = $_POST['id'];
    $repository->deleteNote($noteId);

    header("Location: index.php");
}