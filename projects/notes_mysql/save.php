<?php

$repository = require_once('NotesRepository.php');

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $note = [
        'id' => sanatize($_POST['id'] ?? 0),
        'title' => sanatize($_POST['title']),
        'description' => sanatize($_POST['description'])
    ];

    if ($note['id'])
        $repository->updateNote($note);
    else
        $repository->addNote($note);

    header('Location: index.php');
}

function sanatize($field)
{
    return $field;
}
