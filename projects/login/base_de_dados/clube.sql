-- --------------------------------------------------------
-- Anfitrião:                    localhost
-- Versão do servidor:           5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Versão:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping data for table clube.usuarios: ~0 rows (approximately)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id_usuario`, `usuario`, `senha`, `nome_completo`, `criado_em`) VALUES
	(1, 'admin', '$2y$10$qctQ6jFYonwEMdjM5cEfZ.WGqECca0.StjTVAeirkOl5cL00/Zz4u', 'admin Nome Completo', '2021-08-24 22:53:47'),
	(2, 'joao',  '$2y$10$qctQ6jFYonwEMdjM5cEfZ.WGqECca0.StjTVAeirkOl5cL00/Zz4u',  'joao Nome Completo', '2021-08-24 23:19:42'),
	(3, 'Ana',   '$2y$10$qctQ6jFYonwEMdjM5cEfZ.WGqECca0.StjTVAeirkOl5cL00/Zz4u',   'Ana Nome Completo', '2021-08-24 23:19:53'),
	(4, 'chico', '$2y$10$qctQ6jFYonwEMdjM5cEfZ.WGqECca0.StjTVAeirkOl5cL00/Zz4u', 'chico Nome Completo', '2021-08-24 23:20:23');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
